import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Lang, TranslationElement, Translations } from '../editor/translation.elements.model';

@Component({
  selector: 'app-display-translations',
  templateUrl: './display-translations.component.html',
  styleUrls: ['../editor/editor.component.scss']
})
export class DisplayTranslationsComponent implements OnInit {

  @Input() languages: Set<Lang>;
  @Input() translations: Translations;
  @Input() translationKeys: Set<string>;
  @Input() filter: string;
  @Input() filterMissingTranslations: boolean;


  constructor(private httpClient: HttpClient){}

  ngOnInit(): void {
      this.initTranslationsMap(Lang.FR);
      this.initTranslationsMap(Lang.EN);
  }

  
  getTranslationValue(lang: Lang, key: string): TranslationElement {
    return this.translations.getTranslationValue(lang, key);
  }

  
  initTranslationsMap(lg: Lang): void {
    this.languages.add(lg);
    this.translations.initLang(lg);
    this.httpClient.get<{}>('assets/' + lg + '.json').subscribe(data => {
      this.generateTranslationsMap(data, '', lg);
      // console.log(this.translations);
    });
  }

  
  generateTranslationsMap(data: {}, currentKey: string, lang: Lang):  void {
    if (data && Object.keys(data).length !== JSON.stringify(data).replace(/"/g, '').length) { // manage when data is the last iteration in file
      for ( const key in data  ) {
        if (key !== undefined) {
          this.generateTranslationsMap(data[key], currentKey !== '' ? currentKey + '.' + key : key, lang);
        }
      }
    } else {
      this.translationKeys.add(currentKey);
      this.translations.newEntry(lang, currentKey, data as string);
    }

  }

}
