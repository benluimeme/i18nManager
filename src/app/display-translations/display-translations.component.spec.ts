import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayTranslationsComponent } from './display-translations.component';

describe('DisplayTranslationsComponent', () => {
  let component: DisplayTranslationsComponent;
  let fixture: ComponentFixture<DisplayTranslationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayTranslationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayTranslationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
