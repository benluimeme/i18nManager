import { Component, Input, OnInit } from '@angular/core';
import { TranslationElement } from '../editor/translation.elements.model';

@Component({
  selector: 'app-editor-element',
  templateUrl: './editor-element.component.html',
  styleUrls: ['./editor-element.component.scss']
})
export class EditorElementComponent implements OnInit {
  @Input() element: TranslationElement;
  constructor() { }

  ngOnInit(): void {
  }

  getClass(): string {
    return this.element && this.element.value && this.element.value.trim().length > 0 ? '' : 'no_translation_element';
  }

}
