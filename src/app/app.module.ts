import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 

import { AppComponent } from './app.component';
import { EditorComponent } from './editor/editor.component';
import { HttpClientModule } from '@angular/common/http';
import { EditorElementComponent } from './editor-element/editor-element.component';
import { FilterTranslationKeysPipe } from './filter-translation-keys.pipe';
import { DisplayTranslationsComponent } from './display-translations/display-translations.component';
import { AddTranslationsComponent } from './add-translations/add-translations.component';
import { PrettyPrintPipePipe } from './pretty-print-pipe.pipe';
import { FilterMissingTranslationsPipe } from './filter-missing-translations.pipe';

@NgModule({
  declarations: [
    AppComponent,
    EditorComponent,
    EditorElementComponent,
    FilterTranslationKeysPipe,
    DisplayTranslationsComponent,
    AddTranslationsComponent,
    PrettyPrintPipePipe,
    FilterMissingTranslationsPipe
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule
  ],
  providers: [PrettyPrintPipePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
