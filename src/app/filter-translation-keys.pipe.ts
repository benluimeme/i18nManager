import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterTranslationKeys'
})
export class FilterTranslationKeysPipe implements PipeTransform {

    transform(keys: Set<string>, filter: string): any {
      if (filter && filter.trim().length > 0) {
        return Array.from(keys).filter(k => k.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
      } else {
        return keys;
      }
    }
  }
