 export class TranslationElement {
    lang: Lang;
    key: string;
    value: string;

    constructor(lang: Lang) {
        this.lang = lang;
    }

}

export class TranslationsKeys {
    private lang: Lang;
    private me: Map<string, TranslationElement>;

    constructor(lang: Lang) {
        this.lang = lang;
        this.init();
    }

    getMap(): Map<string, TranslationElement> {
        return this.me;
    }

    get(key: string): TranslationElement {
        return this.me.get(key) ? this.me.get(key) : {key, value: '', lang: this.lang};
    }

    init() {
        this.me = new Map();
    }

    newEntry(key: string, value: string) {
        this.me.set(key, {key, value, lang: this.lang});
    }

    addAll(newTranslations: TranslationsKeys) {
        Array.from(
            newTranslations.getMap().entries())
                .forEach( (entry: [string, TranslationElement]) => this.newEntry(entry[0], entry[1].value));
    }
}

export class Translations {
    me: Map<Lang, TranslationsKeys>;
    constructor() {}

    getMap(): Map<Lang, TranslationsKeys> {
        return this.me;
    }

    initMap() {
        this.me = new Map();
    }

    initLang(l: Lang): void {
        this.me.set(l, new TranslationsKeys(l));
    }

    newEntry(lang: Lang, key: string, value: string): void {
        this.me.get(lang).newEntry(key, value);
    }

    get(l: Lang): TranslationsKeys  {
        return this.me.get(l);
    }

    getTranslationValue(lang: Lang, key: string): TranslationElement {
        return this.me.get(lang).get(key);
    }

    clear(): void {
        this.me.clear();
    }

    addAll(newTranslations: Translations) {
        Array.from(newTranslations.getMap().keys())
                    .forEach( (lang: Lang) => this.get(lang).addAll(newTranslations.get(lang)));
    }
}

export enum Lang {
    FR = 'fr',
    EN = 'en'
}

export enum TranslationSaveEvent {
    SAVE,
    CANCEL
}


export class TranslationEntry {
    key: string;
    translation: TranslationElement[];
  
    constructor(key: string, translation: TranslationElement[]) {
      this.key = key;
      this.translation = translation;
    }
  }