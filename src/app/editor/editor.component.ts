import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Lang, Translations, TranslationSaveEvent, TranslationsKeys } from './translation.elements.model';
import {saveAs} from 'file-saver';
import { PrettyPrintPipePipe } from '../pretty-print-pipe.pipe';
import { AddTranslationsComponent } from '../add-translations/add-translations.component';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {

  @ViewChild(AddTranslationsComponent) addCmp: AddTranslationsComponent;
  translations: Translations = new Translations();
  filter: string;

  languages: Set<Lang> = new Set();
  translationKeys: Set<string> = new Set();
  isVisibleAddTranslation = false;
  filterMissingTranslations = false;

  constructor(
    private httpClient: HttpClient,
    private jsonPretty: PrettyPrintPipePipe){}

  ngOnInit() {
    this.translations.initMap();
  }

  saveTranslations(): void {
    
    this.saveTranslation(Lang.FR);
    this.saveTranslation(Lang.EN);
  }
  
  private saveTranslation(lang: Lang): void {
    const exportedKeys: Set<string> = new Set();
    Array.from(this.translationKeys).forEach(e => exportedKeys.add(e));
    this.httpClient.get<{}>('assets/' + lang + '.json').subscribe(data => { 
      const existing = this.convertTranslationMapToFile(exportedKeys, data, '', lang);
      const finalExport: {} = this.addNewlyCreatedEntries(exportedKeys, existing);
      const stringified = this.jsonPretty.transform(finalExport) as string;
      saveAs(new Blob([stringified]), lang + '.json' );
    });
  }

  addNewlyCreatedEntries(exportedKeys: Set<string>, existing: {}): {} {
    this.languages
          .forEach(l => exportedKeys
                  .forEach(k => {
                    const val = this.translations.get(l).get(k);
                    if (val) { 
                      existing[k] = val.value;
                    }
                  }));
    return existing;
  }

  private convertTranslationMapToFile(exportedKeys: Set<string>, currentData: {}, aggregateKey: string, lang: Lang): {} {
    // manage when data is the last iteration in file
    if (Object.keys(currentData).length !== JSON.stringify(currentData).replace(/"/g, '').length) { 
      const current = {};
      for ( const key in currentData  ) {
        if (key !== undefined) {
          current[key] = this.convertTranslationMapToFile(exportedKeys, currentData[key], aggregateKey !== '' ? aggregateKey + '.' + key : key, lang);
        }
      }
      exportedKeys.delete(aggregateKey);
      return current;
    } else {
      exportedKeys.delete(aggregateKey);
      return this.translations.get(lang).get(aggregateKey).value;
    }
  }

  addTranslation(): void {
    if (this.addCmp ) {
      this.addCmp.addNewTranslationLine();
    }
    this.isVisibleAddTranslation = true;
  }

  manageNewTranslation(res: TranslationSaveEvent) {
    // console.log(res);
    this.isVisibleAddTranslation = false;
  }

  filtering(filter: string) {
    this.filter = filter
  }

  
}
