import { Pipe, PipeTransform } from '@angular/core';
import { Translations, TranslationSaveEvent } from './editor/translation.elements.model';

@Pipe({
  name: 'filterMissingTranslations'
})
export class FilterMissingTranslationsPipe implements PipeTransform {

  transform(existingKeys: Set<string>, ...args: unknown[]): unknown {
    if (args && args[0] && args[1] && (args[0] === true) ) {
        const translations = args[1] as Translations;
        const languages = Array.from(translations.getMap().keys());
        return Array.from(existingKeys).filter(k => languages.map(l => {
            const v = translations.get(l).get(k);
            return v && v.value && v.value.trim().length > 0;
          }).filter(b => b === true).length !== languages.length
        );
    } else {
      return existingKeys;
    }
  }

}
