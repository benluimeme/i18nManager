import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Lang, TranslationElement, Translations, 
      TranslationSaveEvent, TranslationEntry } from '../editor/translation.elements.model';

@Component({
  selector: 'app-add-translations',
  templateUrl: './add-translations.component.html',
  styleUrls: ['../editor/editor.component.scss']
})
export class AddTranslationsComponent implements OnInit {
  @Input() languages: Set<Lang>;
  @Input() translations: Translations;
  @Input() translationKeys: Set<string>;

  @Output() close = new EventEmitter<TranslationSaveEvent>();
  @Output() filter = new EventEmitter<string>();

  newEntries: TranslationEntry[] = [];
  entryAlreadyExists = false;
  existingEntry: string;

  constructor() { }

  ngOnInit(): void {
    this.addNewTranslationLine();
  }

  generateTranslationsMap(data: {}, currentKey: string, lang: Lang):  void {
      this.translationKeys.add(currentKey);
      this.translations.newEntry(lang, currentKey, data as string);
  }

  addNewTranslationLine() {
    this.newEntries.push(
          new TranslationEntry('new_key' + this.newEntries.length, 
                Array.from(this.languages).map(l => new TranslationElement(l))));
  }

  saveNewTranslation(): void {
    const toBeEntries = this.newEntries.map(e => e.key); 
    for ( const key in toBeEntries) {
      const currentKey = toBeEntries[key];
      if (this.translationKeys.has(currentKey)) {
          this.entryAlreadyExists = true;
          this.existingEntry = currentKey;
          this.filter.emit(currentKey);
      }
    }
    if (!this.entryAlreadyExists) {
      this.newEntries.forEach( e => Array.from(this.languages)
          .forEach(l => this.generateTranslationsMap(e.translation
                    .filter(e => e.lang === l)[0].value, e.key, l)));
      this.close.emit(TranslationSaveEvent.SAVE);
      this.newEntries = [];
    } 

  }

  cancelNewTranslation(): void  {
    this.close.emit(TranslationSaveEvent.CANCEL); 
    this.newEntries = [];
  }

  getTranslationValue(lang: Lang, key: string): TranslationElement {
    const mapped = this.newEntries.filter(e => e && e.key === key).map(
          e => this.returnTranslationElement(lang, e));
    return mapped.length === 1 ? mapped[0] : undefined;
  }

  returnTranslationElement(lang: Lang, e: TranslationEntry): TranslationElement {
    const filtered = e.translation.filter(t => t.lang === lang);
    return filtered.length === 1 ? filtered[0] : undefined;
  }


  resetKeyEntry() {
    this.entryAlreadyExists = false;
    this.filter.emit('');
  }

}

