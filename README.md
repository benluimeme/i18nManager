# i18nManager
Angular project to edit i18n json file. Deploys automatically to gitlab pages using gitlab CI.

# See this app in action
Demo  here : https://benluimeme.gitlab.io/i18nManager/

# /!\ Important!
Currently 2 different branches host 2 different behaviours:
* 'main' branch launches app with 2 sample files that could not be modified
* 'using_gitlab_api' branch launches app with connection to this gitlab repo
* if you wan to use this app, clone according to your needs

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
